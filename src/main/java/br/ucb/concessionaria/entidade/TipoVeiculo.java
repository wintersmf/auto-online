package br.ucb.concessionaria.entidade;

public class TipoVeiculo {

	private Integer id;
	private String tipoVeiculo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoVeiculo() {
		return tipoVeiculo;
	}
	public void setTipoVeiculo(String tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	} 
	
}
