package br.ucb.concessionaria.entidade;

public class Veiculo {
 
    private String numeroChassi;
    private String numeroPlaca;
    private String fabricante;
    private String modelo;
    private String anoFabricacao;
    private String cor;
    private Integer id;
    private TipoVeiculo tipoVeiculo;
    private String quantidadePassageiros;
    private String numeroEixos;
    private String numeroAndares;
	private String potencia;
	private String quantidadePortas;
	private String aceleracao;
    private String capacidadeCarga;
    private String biTrem;

	public String getNumeroChassi() {
		return numeroChassi;
	}
	
	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}
	
	public String getNumeroPlaca() {
		return numeroPlaca;
	}
	
	public void setNumeroPlaca(String numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}
	
	public String getFabricante() {
		return fabricante;
	}
	
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getAnoFabricacao() {
		return anoFabricacao;
	}
	
	public void setAnoFabricacao(String anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	
	public String getCor() {
		return cor;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}
	
	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	
	public String getQuantidadePassageiros() {
		return quantidadePassageiros;
	}
	
	public void setQuantidadePassageiros(String quantidadePassageiros) {
		this.quantidadePassageiros = quantidadePassageiros;
	}
	
	public String getNumeroEixos() {
		return numeroEixos;
	}
	
	public void setNumeroEixos(String numeroEixos) {
		this.numeroEixos = numeroEixos;
	}
	
	public String getNumeroAndares() {
		return numeroAndares;
	}
	
	public void setNumeroAndares(String numeroAndares) {
		this.numeroAndares = numeroAndares;
	}
	
	public String getPotencia() {
		return potencia;
	}
	
	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}
	
	public String getQuantidadePortas() {
		return quantidadePortas;
	}
	
	public void setQuantidadePortas(String quantidadePortas) {
		this.quantidadePortas = quantidadePortas;
	}
	
	public String getAceleracao() {
		return aceleracao;
	}
	
	public void setAceleracao(String aceleracao) {
		this.aceleracao = aceleracao;
	}
	
	public String getCapacidadeCarga() {
		return capacidadeCarga;
	}
	
	public void setCapacidadeCarga(String capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}
	
	public String getBiTrem() {
		return biTrem;
	}
	
	public void setBiTrem(String biTrem) {
		this.biTrem = biTrem;
	}

}
