package br.ucb.concessionaria.logica;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucb.concessionaria.entidade.TipoVeiculo;
import br.ucb.concessionaria.entidade.Veiculo;
import br.ucb.concessionaria.persistencia.TipoVeiculoDao;
import br.ucb.concessionaria.persistencia.VeiculoDao;

public class AdicionaVeiculoLogic implements Logica {

	private TipoVeiculoDao tipoVeiculoDao;
	
	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {

		tipoVeiculoDao = new TipoVeiculoDao();
		Veiculo veiculo = new Veiculo();
		veiculo.setFabricante(req.getParameter("fabricante"));
		veiculo.setModelo(req.getParameter("modelo"));
		veiculo.setCor(req.getParameter("cor"));
		veiculo.setAnoFabricacao(req.getParameter("anoFabricacao"));
		veiculo.setNumeroChassi(req.getParameter("numeroChassi"));
		veiculo.setNumeroPlaca(req.getParameter("numeroPlaca"));
		String idTipoVeiculo = req.getParameter("tipoVeiculo");
		if (idTipoVeiculo != null) {
			veiculo.setTipoVeiculo(tipoVeiculoDao.consultaTipoVeiculoPeloId(Integer.parseInt(idTipoVeiculo)));
		}
		if (idTipoVeiculo.equals("1")) {
			veiculo.setPotencia(req.getParameter("potencia"));
			veiculo.setQuantidadePortas(req.getParameter("quantidadePortas"));
			
			VeiculoDao dao = new VeiculoDao();
			dao.adicionar(veiculo);
		}
		if (idTipoVeiculo.equals("2")) {
			veiculo.setAceleracao(req.getParameter("aceleracao"));
			veiculo.setPotencia(req.getParameter("potencia"));
			
			VeiculoDao dao = new VeiculoDao();
			dao.adicionar(veiculo);
		}
		if (idTipoVeiculo.equals("3")) {
			veiculo.setBiTrem(req.getParameter("biTrem"));
			veiculo.setCapacidadeCarga(req.getParameter("capacidadeCarga"));
			veiculo.setNumeroEixos(req.getParameter("numeroEixos"));
		
			VeiculoDao dao = new VeiculoDao();
			dao.adicionar(veiculo);
		}
		if (idTipoVeiculo.equals("4")) {
			veiculo.setNumeroAndares(req.getParameter("numeroAndares"));
			veiculo.setNumeroEixos(req.getParameter("numeroEixos"));
			veiculo.setQuantidadePassageiros(req.getParameter("quantidadePassageiros"));
		
			VeiculoDao dao = new VeiculoDao();
			dao.adicionar(veiculo);
		}
		
		RequestDispatcher rd = req.getRequestDispatcher("/mvc?logica=ListaVeiculosLogic");
		rd.forward(req, res);
		System.out.println("Adicionando veiculo ..." + veiculo.getModelo());

		return "/mvc?logica=ListaVeiculosLogic";

	}

}