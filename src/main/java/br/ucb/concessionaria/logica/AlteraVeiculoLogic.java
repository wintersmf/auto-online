package br.ucb.concessionaria.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucb.concessionaria.entidade.TipoVeiculo;
import br.ucb.concessionaria.entidade.Veiculo;
import br.ucb.concessionaria.persistencia.TipoVeiculoDao;
import br.ucb.concessionaria.persistencia.VeiculoDao;

public class AlteraVeiculoLogic implements Logica {

	private static final String PARAM_ALTERA = "paramAltera";
	private static final String SALVAR = "salvar";

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String tipo = req.getParameter(PARAM_ALTERA);
		if (tipo != null && tipo.equals(SALVAR)) {
			return salvarVeiculo(req, resp);
		} else {
			return popula(req, resp);
		}

	}

	private String salvarVeiculo(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String id = req.getParameter("id");
		if ( id !=  null && !id.equals("")) {
			return alterar(req, resp);
		} else {
			return adicionar(req, resp);
		}
	}

	public String popula(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		Veiculo veiculo = new Veiculo();
		VeiculoDao dao = new VeiculoDao();
		String id = (req.getParameter("id"));
		veiculo = dao.consultaVeiculoPeloId(id);
		
		List<TipoVeiculo> tipoVeiculos = new TipoVeiculoDao().getLista();
		req.setAttribute("tipoVeiculos", tipoVeiculos);
		
		req.setAttribute("tipoVeiculoSelecionado",veiculo.getTipoVeiculo().getTipoVeiculo());
		
		System.out.println("buscando o veiculo:" + id);
		req.setAttribute("veiculo", veiculo);

				
		return "/altera-veiculo.jsp";

	}

	public String alterar(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		TipoVeiculoDao tipoVeiculoDao = new TipoVeiculoDao();
		VeiculoDao daoConsulta = new VeiculoDao();
		Veiculo veiculo = new Veiculo();
		String id = (req.getParameter("id"));
		veiculo = daoConsulta.consultaVeiculoPeloId(id);
		veiculo.setFabricante(req.getParameter("fabricante"));
		veiculo.setModelo(req.getParameter("modelo"));
		veiculo.setCor(req.getParameter("cor"));
		veiculo.setAnoFabricacao(req.getParameter("anoFabricacao"));
		veiculo.setNumeroChassi(req.getParameter("numeroChassi"));
		veiculo.setNumeroPlaca(req.getParameter("numeroPlaca"));
		String idTipoVeiculo = req.getParameter("tipoVeiculo");
		if (idTipoVeiculo != null) {
			veiculo.setTipoVeiculo(tipoVeiculoDao.consultaTipoVeiculoPeloId(Integer.parseInt(idTipoVeiculo)));
		}
		if (idTipoVeiculo.equals("1")) {
			veiculo.setPotencia(req.getParameter("potencia"));
			veiculo.setQuantidadePortas(req.getParameter("quantidadePortas"));
			
			VeiculoDao dao = new VeiculoDao();
			dao.alterar(veiculo);
		}
		if (idTipoVeiculo.equals("2")) {
			veiculo.setAceleracao(req.getParameter("aceleracao"));
			veiculo.setPotencia(req.getParameter("potencia"));
			
			VeiculoDao dao = new VeiculoDao();
			dao.alterar(veiculo);
		}
		if (idTipoVeiculo.equals("3")) {
			veiculo.setBiTrem(req.getParameter("biTrem"));
			veiculo.setCapacidadeCarga(req.getParameter("capacidadeCarga"));
			veiculo.setNumeroEixos(req.getParameter("numeroEixos"));
		
			VeiculoDao dao = new VeiculoDao();
			dao.alterar(veiculo);
		}
		if (idTipoVeiculo.equals("4")) {
			veiculo.setNumeroAndares(req.getParameter("numeroAndares"));
			veiculo.setNumeroEixos(req.getParameter("numeroEixos"));
			veiculo.setQuantidadePassageiros(req.getParameter("quantidadePassageiros"));
		
			VeiculoDao dao = new VeiculoDao();
			dao.alterar(veiculo);
		}
	
		System.out.println("Alterando veiculo... " + veiculo.getModelo());

		return "mvc?logica=ListaVeiculosLogic";
	}

	public String adicionar(HttpServletRequest req, HttpServletResponse resp) throws Exception {

		Veiculo veiculo = new Veiculo();
		veiculo.setFabricante(req.getParameter("fabricante"));
		veiculo.setModelo(req.getParameter("modelo"));
		veiculo.setCor(req.getParameter("cor"));
		veiculo.setPotencia(req.getParameter("potencia"));
		veiculo.setNumeroPlaca(req.getParameter("numeroPlaca"));
		veiculo.setAnoFabricacao(req.getParameter("anoFabricacao"));
		veiculo.setNumeroChassi(req.getParameter("numeroChassi"));
		veiculo.setQuantidadePortas(req.getParameter("quantidadePortas"));
		TipoVeiculo tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setTipoVeiculo(req.getParameter("tipoVeiculo"));
		
		VeiculoDao dao = new VeiculoDao();
		dao.adicionar(veiculo);
		System.out.println("Alterando veiculo... " + veiculo.getModelo());

		return "mvc?logica=ListaVeiculosLogic";
	}

}
