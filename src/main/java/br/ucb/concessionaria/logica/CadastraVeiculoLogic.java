package br.ucb.concessionaria.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucb.concessionaria.entidade.TipoVeiculo;
import br.ucb.concessionaria.persistencia.TipoVeiculoDao;

public class CadastraVeiculoLogic implements Logica {

	private TipoVeiculoDao tipoVeiculoDao;

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		List<TipoVeiculo> tipoVeiculos = new TipoVeiculoDao().getLista();
		req.setAttribute("tipoVeiculos", tipoVeiculos);

		return "cadastra-veiculo.jsp";
	}

}
