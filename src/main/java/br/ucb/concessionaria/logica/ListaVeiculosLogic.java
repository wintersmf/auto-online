package br.ucb.concessionaria.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucb.concessionaria.entidade.Veiculo;
import br.ucb.concessionaria.persistencia.VeiculoDao;

public class ListaVeiculosLogic implements Logica {
	public String executa(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		List<Veiculo> veiculos = new VeiculoDao().getLista();
		req.setAttribute("veiculos", veiculos);
		
		return "lista-veiculos-taglib.jsp";
		
	}
}