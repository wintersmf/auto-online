package br.ucb.concessionaria.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ucb.concessionaria.entidade.Veiculo;
import br.ucb.concessionaria.persistencia.VeiculoDao;

public class RemoveVeiculoLogic implements Logica {
	public String executa(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		String id = (req.getParameter("id"));
		Veiculo veiculo = new Veiculo();
		veiculo.setId(Integer.parseInt(id));
		VeiculoDao dao = new VeiculoDao();
		dao.remover(veiculo);
		System.out.println("Excluindo veiculo... logica..");
	
		return "mvc?logica=ListaVeiculosLogic";
	}
}
