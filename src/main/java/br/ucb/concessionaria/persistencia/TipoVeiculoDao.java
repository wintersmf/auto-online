package br.ucb.concessionaria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ucb.concessionaria.entidade.TipoVeiculo;
import br.ucb.concessionaria.entidade.Veiculo;

public class TipoVeiculoDao {

	private final Connection connection;

	public TipoVeiculoDao() {
		BancoDeDados bd = new BancoDeDados();
		bd.conectar();
		this.connection = bd.getConnection();
	}
	
	public TipoVeiculo consultaTipoVeiculoPeloId(Integer id){
		try{
			TipoVeiculo tipoVeiculo = new TipoVeiculo();
			PreparedStatement stmt = this.connection.
					prepareStatement("select * from tipoVeiculos where id=?");
			
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				tipoVeiculo.setTipoVeiculo(rs.getString("tipoVeiculo"));
				tipoVeiculo.setId(rs.getInt("id"));
			}

			rs.close();
			stmt.close();
			return tipoVeiculo;
		} catch(SQLException e){
			throw new RuntimeException(e);

		}

	}

	
	public List<TipoVeiculo> getLista(){
	try{
		List<TipoVeiculo> tipoVeiculos = new ArrayList<TipoVeiculo>();
		PreparedStatement stmt = this.connection.
				prepareStatement("select * from tipoVeiculos");
		ResultSet rs = stmt.executeQuery();

		while(rs.next()) {

			TipoVeiculo tipoVeiculo = new TipoVeiculo();
			tipoVeiculo.setId(rs.getInt("Id"));
			tipoVeiculo.setTipoVeiculo(rs.getString("tipoVeiculo"));
			tipoVeiculos.add(tipoVeiculo);
		}
		rs.close();
		stmt.close();
		return tipoVeiculos;
	} catch(SQLException e){
		throw new RuntimeException(e);
	}
}
	
}
