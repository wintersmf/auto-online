package br.ucb.concessionaria.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ucb.concessionaria.entidade.TipoVeiculo;
import br.ucb.concessionaria.entidade.Veiculo;

/**
 * @author Winter Is Coming
 *
 */
public class VeiculoDao {

	
	private final Connection connection;

	public VeiculoDao() {
		BancoDeDados bd = new BancoDeDados();
		bd.conectar();
		this.connection = bd.getConnection();
	}

	public void adicionar(Veiculo veiculo) {
		String sql = "insert into veiculos"
				+ "(modelo, fabricante, numeroPlaca, anoFabricacao, quantidadePortas, cor,"
				+ "numeroChassi, potencia, tipoVeiculo, aceleracao, biTrem,"
				+ "capacidadeCarga, numeroAndares, numeroEixos)"
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			
			PreparedStatement stmt = connection.prepareStatement(sql);
			
			stmt.setString(1, veiculo.getModelo());
			stmt.setString(2, veiculo.getFabricante());
			stmt.setString(3, veiculo.getNumeroPlaca());
			stmt.setString(4, veiculo.getAnoFabricacao());
			stmt.setString(5, veiculo.getQuantidadePortas());
			stmt.setString(6, veiculo.getCor());
			stmt.setString(7, veiculo.getNumeroChassi());
			stmt.setString(8, veiculo.getPotencia());
			stmt.setString(9, veiculo.getTipoVeiculo().getTipoVeiculo());
			stmt.setString(10, veiculo.getAceleracao());
			stmt.setString(11, veiculo.getBiTrem());
			stmt.setString(12, veiculo.getCapacidadeCarga());
			stmt.setString(13, veiculo.getNumeroAndares());
			stmt.setString(14, veiculo.getNumeroEixos());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void alterar(Veiculo veiculo) {
		String sql = "update veiculos set modelo=?,fabricante=?,numeroPlaca=?,"
				+ "anoFabricacao=?,quantidadePortas=?,cor=?,numeroChassi=?,"
				+ "potencia=?, tipoVeiculo=?, aceleracao=?, biTrem=?,"
				+ "capacidadeCarga=?, numeroAndares=?, numeroEixos=? where id=?";

		try {

			PreparedStatement stmt = connection.prepareStatement(sql);
			
			stmt.setString(1, veiculo.getModelo());
			stmt.setString(2, veiculo.getFabricante());
			stmt.setString(3, veiculo.getNumeroPlaca());
			stmt.setString(4, veiculo.getAnoFabricacao());
			stmt.setString(5, veiculo.getQuantidadePortas());
			stmt.setString(6, veiculo.getCor());
			stmt.setString(7, veiculo.getNumeroChassi());
			stmt.setString(8, veiculo.getPotencia());
			stmt.setString(9, veiculo.getTipoVeiculo().getTipoVeiculo());
			stmt.setString(10, veiculo.getAceleracao());
			stmt.setString(11, veiculo.getBiTrem());
			stmt.setString(12, veiculo.getCapacidadeCarga());
			stmt.setString(13, veiculo.getNumeroAndares());
			stmt.setString(14, veiculo.getNumeroEixos());
			stmt.setInt(15, veiculo.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remover(Veiculo veiculo) {
		try {
			
			PreparedStatement stmt = connection
					.prepareStatement("delete from veiculos where id=?");
			
			stmt.setInt(1, veiculo.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}


	public Veiculo consultaVeiculoPeloId(String id){
		try{
			Veiculo veiculo = new Veiculo();
			PreparedStatement stmt = this.connection.
					prepareStatement("select * from veiculos where id=?");
			
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				
				veiculo.setFabricante(rs.getString("fabricante"));
				veiculo.setModelo(rs.getString("modelo"));
				veiculo.setCor(rs.getString("cor"));
				veiculo.setPotencia(rs.getString("potencia"));
				veiculo.setNumeroPlaca(rs.getString("numeroPlaca"));
				veiculo.setAnoFabricacao(rs.getString("anoFabricacao"));
				veiculo.setNumeroChassi(rs.getString("numeroChassi"));
				veiculo.setQuantidadePortas(rs.getString("quantidadePortas"));
				veiculo.setId(rs.getInt("Id"));
				veiculo.setAceleracao(rs.getString("aceleracao"));
				veiculo.setBiTrem(rs.getString("biTrem"));
				veiculo.setCapacidadeCarga(rs.getString("capacidadeCarga"));
				veiculo.setNumeroAndares(rs.getString("numeroAndares"));
				veiculo.setNumeroEixos(rs.getString("numeroEixos"));
				TipoVeiculo tipoVeiculo = new TipoVeiculo();
				tipoVeiculo.setTipoVeiculo(rs.getString("tipoVeiculo"));
				veiculo.setTipoVeiculo(tipoVeiculo);
				
			}

			rs.close();
			stmt.close();
			return veiculo;
		} catch(SQLException e){
			throw new RuntimeException(e);

		}

	}

	public List<Veiculo> getLista(){
		try{
			List<Veiculo> veiculos = new ArrayList<Veiculo>();
			PreparedStatement stmt = this.connection.
					prepareStatement("select * from veiculos");
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {

				Veiculo veiculo = new Veiculo();
				veiculo.setFabricante(rs.getString("fabricante"));
				veiculo.setModelo(rs.getString("modelo"));
				veiculo.setCor(rs.getString("cor"));
				veiculo.setNumeroPlaca(rs.getString("numeroPlaca"));
				veiculo.setAnoFabricacao(rs.getString("anoFabricacao"));
				veiculo.setNumeroChassi(rs.getString("numeroChassi"));
				veiculo.setId(rs.getInt("Id"));
				veiculo.setAceleracao(rs.getString("aceleracao"));
				veiculo.setBiTrem(rs.getString("biTrem"));
				veiculo.setCapacidadeCarga(rs.getString("capacidadeCarga"));
				veiculo.setNumeroAndares(rs.getString("numeroAndares"));
				veiculo.setNumeroEixos(rs.getString("numeroEixos"));
				TipoVeiculo tipoVeiculo = new TipoVeiculo();
				tipoVeiculo.setTipoVeiculo(rs.getString("tipoVeiculo"));
				veiculo.setTipoVeiculo(tipoVeiculo);
				veiculos.add(veiculo);

			}

			rs.close();
			stmt.close();
			return veiculos;
		} catch(SQLException e){
			throw new RuntimeException(e);

		}

	}


}
