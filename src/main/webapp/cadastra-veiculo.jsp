<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Adiciona Veiculo</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	// Espera o carregamento da p�gina para fazer o bind dos eventos
	$(function() {

		var elem = document.getElementById("tipoVeiculo");
		var divCarro = document.getElementById("carro");
		var divCaminhao = document.getElementById("caminhao");
		var divMoto = document.getElementById("moto");
		var divOnibus = document.getElementById("onibus");

		var ajustarCampos = function() {
			if (elem.value == '1') {
				divCarro.style.display = "block";
				divCaminhao.style.display = "none";
				divMoto.style.display = "none";
				divOnibus.style.display = "none";
			} else if (elem.value == '2') {
				divCarro.style.display = "none";
				divCaminhao.style.display = "none";
				divMoto.style.display = "block";
				divOnibus.style.display = "none";
			} else if (elem.value == '3') {
				divCarro.style.display = "none";
				divCaminhao.style.display = "block";
				divMoto.style.display = "none";
				divOnibus.style.display = "none";
			} else {
				divCarro.style.display = "none";
				divCaminhao.style.display = "none";
				divMoto.style.display = "none";
				divOnibus.style.display = "block";
			}

		};

		elem.onchange = ajustarCampos;

		ajustarCampos();

	})
</script>

<style>
div.fabricante {
	position: absolute;
	top: 250px;
	right: 970px;
	width: 280px;
	height: 30px;
}

div.modelo {
	position: absolute;
	top: 312px;
	right: 970px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.cor {
	position: absolute;
	top: 375px;
	right: 970px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.numeroPlaca {
	position: absolute;
	top: 250px;
	right: 660px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.anoFabricacao {
	position: absolute;
	top: 312px;
	right: 660px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.numeroChassi {
	position: absolute;
	top: 375px;
	right: 660px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.potencia {
	position: absolute;
	top: 250px;
	right: 350px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.quantidadePortas {
	top: 410px;
	right: 980px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.aceleracao {
	position: absolute;
	top: 312px;
	right: 350px;
	width: 280px;
	height: 30px;
}

div.numeroEixos {
	position: absolute;
	top: 250px;
	right: 350px;
	width: 280px;
	height: 30px;
	width: 280px;
}

div.capacidadeCarga {
	position: absolute;
	top: 312px;
	right: 350px;
	width: 280px;
	height: 30px;
}

div.biTrem {
	position: absolute;
	top: 375px;
	right: 350px;
	width: 280px;
	height: 30px;
}

div.numeroAndares {
	position: absolute;
	top: 312px;
	right: 350px;
	width: 280px;
	height: 30px;
}

div.quantidadePassageiros {
	position: absolute;
	top: 375px;
	right: 350px;
	width: 280px;
	height: 30px;
}

.wrapper {
	text-align: right;
}

div.voltar {
	position: absolute;
	top: 500px;
	right: 400px;
	width: 25px;
	height: 25px;
}

div.absolute {
	position: absolute;
	top: 500px;
	right: 280px;
	width: 25px;
	height: 25px;
}

.footer {
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	height: 100px;
}

body {
	background: #7cb5f9;
}

.center-on-page {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

h1 {
	text-align: center;
}
/* Reset Select */
select {
	-webkit-appearance: none;
	-moz-appearance: none;
	-ms-appearance: none;
	appearance: none;
	outline: 0;
	box-shadow: none;
	border: 0 !important;
	background: #2c3e50;
	background-image: none;
}
/* Custom Select */
.select {
	position: absolute;
	display: block;
	width: 20em;
	height: 3em;
	line-height: 3;
	right: 970px;
	background: #2c3e50;
	overflow: hidden;
	border-radius: .25em;
}

select {
	width: 100%;
	height: 100%;
	margin: 0;
	padding: 0 0 0 .5em;
	color: #fff;
	cursor: pointer;
}

select::-ms-expand {
	display: none;
}
/* Arrow */
.select::after {
	content: '\25BC';
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	padding: 0 1em;
	background: #34495e;
	pointer-events: none;
}
/* Transition */
.select:hover::after {
	color: #f39c12;
}

.select::after {
	-webkit-transition: .25s all ease;
	-o-transition: .25s all ease;
	transition: .25s all ease;
}
</style>

</head>
<body>
	<form action="mvc">

		<center>
			<c:import url="cabecalho.jsp" />
		</center>

		<div class="select">
			<input type="hidden" name="logica" value="AdicionaVeiculoLogic">

			<select class="form-group" id="tipoVeiculo" name="tipoVeiculo">
				<c:forEach var="tipoVeiculo" items="${tipoVeiculos}">
					<option value="${tipoVeiculo.id}">${tipoVeiculo.tipoVeiculo}</option>
				</c:forEach>
			</select>
		</div>

		<div class="form-group">

			<input name="id" class="form-control" id="id" type="hidden"
				value="${veiculo.id}">

			<div class="fabricante">
				<label for="fabricante">Fabricante: </label> <input
					name="fabricante" class="form-control" id="fabricante" type="text"
					value="${veiculo.fabricante}">
			</div>

			<div class="modelo">
				<label for="modelo">Modelo: </label> <input class="form-control"
					name="modelo" id="modelo" type="text" value="${veiculo.modelo}">
			</div>
			<div class="cor">
				<label for="cor">Cor: </label> <input class="form-control"
					name="cor" id="cor" type="text" value="${veiculo.cor}">
			</div>

			<div class="numeroPlaca">
				<label for="numeroPlaca">Numero da Placa: </label> <input
					class="form-control" name="numeroPlaca" id="numeroPlaca"
					type="text" value="${veiculo.numeroPlaca}">
			</div>
		</div>

		<div class="anoFabricacao">
			<label for="anoFabricacao">Ano de Fabrica��o: </label> <input
				class="form-control" name="anoFabricacao" id="anoFabricacao"
				type="text" value="${veiculo.anoFabricacao}">
		</div>

		<div class="numeroChassi">
			<label for="numeroChassi">Numero do Chassi: </label> <input
				class="form-control" name="numeroChassi" id="numeroChassi"
				type="text" value="${veiculo.numeroChassi}">
		</div>


		<div id="carro">
			<div class="potencia">
				<label for="potencia">Pot�ncia: </label> <input class="form-control"
					name="potencia" id="potencia" type="text"
					value="${veiculo.potencia}">

				<div class="quantidadePortas">
					<label for="quantidadePortas">Quantidade de Portas: </label> <input
						class="form-control" name="quantidadePortas" id="quantidadePortas"
						type="text" value="${veiculo.quantidadePortas}">
				</div>
			</div>
		</div>

		<div id="moto">
			<div class="potencia">
				<label for="potencia">Pot�ncia: </label> <input class="form-control"
					name="potencia" id="potencia" type="text"
					value="${veiculo.potencia}">
			</div>

			<div class="aceleracao">
				<label for="aceleracao">Acelera��o: </label> <input
					class="form-control" name="potencia" id="potencia" type="text"
					value="${veiculo.aceleracao}">
			</div>
		</div>

		<div id="caminhao">
			<div class="numeroEixos">
				<label for="numeroEixos">Quantidade de Eixos: </label> <input
					class="form-control" name="numeroEixos" id="numeroEixos"
					type="text" value="${veiculo.numeroEixos}">
			</div>

			<div class="capacidadeCarga">
				<label for="capacidadeCarga">Capacidade de Carga: </label> <input
					class="form-control" name="capacidadeCarga" id="capacidadeCarga"
					type="text" value="${veiculo.capacidadeCarga}">
			</div>

			<div class="biTrem">
				<label for="biTrem">Caminh�o Bi-Trem: </label> <input
					class="form-control" name="biTrem" id="biTrem" type="text"
					value="${veiculo.biTrem}">
			</div>
		</div>

		<div id="onibus">
			<div class="numeroEixos">
				<label for="numeroEixos">Quantidade de Eixos: </label> <input
					class="form-control" name="numeroEixos" id="numeroEixos"
					type="text" value="${veiculo.numeroEixos}">
			</div>
			<div class="numeroAndares">
				<label for="numeroAndares">Quantidade de andar(es): </label> <input
					class="form-control" name="numeroAndares" id="numeroAndares"
					type="text" value="${veiculo.numeroAndares}">
			</div>
			<div class="quantidadePassageiros">
				<label for="quantidadePassageiros">Quantidade de
					Passageiros: </label> <input class="form-control"
					name="quantidadePassageiros" id="quantidadePassageiros" type="text"
					value="${veiculo.quantidadePassageiros}">
			</div>
		</div>

		<input type="hidden" name="logica" value="AdicionaVeiculoLogic" /> <br>
		<div class="absolute">
			<input type="submit" value="Salvar" class="btn btn-success" />
		</div>
		<input type="hidden" name="logica" value="ListaVeiculosLogic" /> <br>
		<div class="voltar">
			<input type="submit" value="Cancelar" class="btn btn-danger" />
		</div>

	</form>


	<div class="footer">
		<c:import url="rodape.jsp" />
		</footer>
	</div>
</body>
</html>
