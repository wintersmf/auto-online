<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<title>Lista Veiculos</title>
</head>
<style>
body {
	background: #7cb5f9;
}

.center-on-page {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

h1 {
	text-align: center;
}

table {
	position: absolute;
	left: 350px;
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 50%;
	font-family: arial, sans-serif;
}

td, th {
	border: 2px solid #000000;
	text-align: left;
	padding: 7px;
}

tr:nth-child(even) {
	background-color: #ffffff;
}

.glyphicon-trash {
	font-size: 15px;
	color: #FF0000;
}

.glyphicon-pencil {
	font-size: 15px;
	color: #000000;
}

.glyphicon-plus {
	position: absolute;
	top: 788px;
	left: 855px;
	margin: 0px;
	padding: 30px;
	font-size: 35px;
	color: #000000;
}

div.menu {
	position: absolute;
	top: 789px;
	left: 895px;
	margin: 0px;
	padding: 30px;
	font-size: 35px;
	color: #000000;
}

.glyphicon-th-list {
	font-size: 15px;
	color: #1E90FF;
}

.footer {
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	height: 100px;
}
</style>
<body>
	<form>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

		<center>
			<c:import url="cabecalho.jsp" />
		</center>

		<table>
			</h2>
			<tr>
				<th>Tipo Veiculo</th>
				<th>Fabricante</th>
				<th>Modelo</th>
				<th>Cor</th>
				<th>Ano Fabricação</th>
				<th>Placa</th>
				<th>Chassi</th>
				<th></th>
			</tr>
			<c:forEach var="veiculo" items="${veiculos}">
				<tr>
					<td>${veiculo.tipoVeiculo.tipoVeiculo}</td>
					<td>${veiculo.fabricante}</td>
					<td>${veiculo.modelo}</td>
					<td>${veiculo.cor}</td>
					<td>${veiculo.anoFabricacao}</td>
					<td>${veiculo.numeroPlaca}</td>
					<td>${veiculo.numeroChassi}</td>
					<td><center>
							<a
								href="mvc?logica=AlteraVeiculoLogic&paramAltera=popula&id=${veiculo.id}"><span
								class="glyphicon glyphicon-pencil"></span></a> <a
								href="mvc?logica=RemoveVeiculoLogic&id=${veiculo.id}"> <span
								class="glyphicon glyphicon-trash"></span></a>

							<!-- <a href="mvc?logica=DetalhaCarroLogic&id=${carro.id}"> <span
								class="glyphicon-th-list"></span></a>-->

						</center></td>
				</tr>
			</c:forEach>
		</table>

		<a href="mvc?logica=CadastraVeiculoLogic"><span
			class="glyphicon-plus"></span></a>
	
		<div class="menu">
			<a href="http://localhost:8080/auto-online">
				<button type="button" class="btn btn-warning">Menu</button>
			</a>
		</div>
		<div class="footer">
			<c:import url="rodape.jsp" />
		</div>
</body>
</html>