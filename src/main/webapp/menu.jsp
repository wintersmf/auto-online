<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Projeto de concession�ria</title>

<style>
div.sair {
	position: absolute;
	top: 322px;
	left: 230px;
	margin: 0px;
	padding: 0px;
	font-size: 35px;
	color: #000000;
}

div.listar {
	position: absolute;
	top: 320px;
	left: 180px;
	margin: 0px;
	padding: 0px;
	font-size: 35px;
	color: #000000;
}

div.cadastrar {
	position: absolute;
	top: 320px;
	left: 100px;
	margin: 0px;
	padding: 0px;
	font-size: 35px;
	color: #000000;
}

body {
	background: #7cb5f9;
}

.center-on-page {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
	<center>
		<c:import url="cabecalho.jsp" />
	</center>

	<p>
	<h3>Ol�,</h3>
	<h3>
		<b><c:out value="${pageContext.request.remoteUser}" /></b><br>
	</h3>
	<h3>Bem vindo ao menu da concession�ria!</h3>
	</p>

	</a>

	<h3>A��es:</h3>

	<div class="sair">
		<form action="logout" method="post">
			<input type="submit" value="Logout" /> <input type="hidden"
				name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>


	<div class="cadastrar">
		<a
			href="http://localhost:8080/auto-online/mvc?logica=CadastraVeiculoLogic">
			<button class="btn btn-danger navbar-btn">Cadastrar</button>
	</div>
	<div class="listar">
		<a
			href="http://localhost:8080/auto-online/mvc?logica=ListaVeiculosLogic">
			<button class="btn btn-danger navbar-btn">Listar</button>
		</a>
	</div>

	<center>
		<c:import url="rodape.jsp" />
	</center>
</body>
</html>